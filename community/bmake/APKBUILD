# Maintainer: Sören Tempel <soeren+alpine@soeren-tempel.net>
pkgname=bmake
pkgver=20200318
pkgrel=0
pkgdesc="Portable version of the NetBSD make build tool"
url="http://www.crufty.net/help/sjg/bmake.html"
arch="all"
license="BSD-2-Clause"
subpackages="$pkgname-doc"
source="http://www.crufty.net/ftp/pub/sjg/bmake-$pkgver.tar.gz
	install-sh.patch
	separate-tests.patch"
builddir="$srcdir/$pkgname"

# Reset MAKEFLAGS since it might contain options not supported
# by bmake. This is, for instance, the case on the builders.
export MAKEFLAGS="-j${JOBS:-1}"

build() {
	sh ./boot-strap --with-default-sys-path=/usr/share/mk op=build
}

check() {
	sh ./boot-strap op=test
}

package() {
	sh ./boot-strap --prefix=/usr --with-mksrc=/usr/share/mk \
			--install-destdir="$pkgdir" op=install

	rm -rf "$pkgdir"/usr/share/man/cat1
	install -Dm644 bmake.1 \
		"$pkgdir"/usr/share/man/man1/bmake.1

	mkdir -p "$pkgdir"/usr/share/doc/$pkgname/
	install -m644 README ChangeLog \
		"$pkgdir"/usr/share/doc/$pkgname/
}

sha512sums="a405226e8f4a9a985ed00dc359390da7890d70f8a7b7e134db05da34ebc3c6ee2373497a9e44e0d75bb64edd81f860fd0cc204bdf7f392dd4e9d2e6a5fe9e507  bmake-20200318.tar.gz
0de9022a2991c5ef02c09ab592a3e2d218cd0bbf58e54f21bc7694110f3dd9e4589bf2b3d241fd167fb220b425007863f20e71e141b4f65bf92d305ba94209da  install-sh.patch
04217b04aca4252f54c836e982d95106a09166370f84fa672c418d1b1799adb9697f5ac9eb10a6ee3a8527e39196a37ad92bb5945733407bf9ec1a7f223183bb  separate-tests.patch"
